import 'package:mystiq/manifest.dart' as core;
import 'package:mystiq/mystiq.dart' as mystiq;
import 'package:mystiq_plugin_get_it/mystiq_plugin_get_it.dart';
import 'package:mystiq_plugin_hive/mystiq_plugin_hive.dart';
import 'package:mystiq_plugin_sanity/mystiq_plugin_sanity.dart';
import 'package:mystiq_uikit/mystiq_uikit.dart' as ui;

void main() {
  final content = SanityContentPlugin(
      dataset: 'production',
      projectId: 'obhivbh2',
      token:
          'skl4ttaz4nbtyl200tQNYXeNFYUJsq04MKuurZP5ST0gIx3KcMINFHvqbCHZrxgLGeYWNAtelyPDU6iyTqK4cVmGxt2zEiarDPFFBeCkr4GEK2dDG0ff1UafW0SzXnSJAqEeRPEoGXLDoHYObrbiLUJAi1vfTCXvv5WmMv1BiaLfHErEqLeZ');
  final storage = HiveStoragePlugin(
    keySpace: 'flutter_app',
  );
  final dependency = GetItDependencyInjectionPlugin();

  mystiq.runApp(
    features: [
      core.manifest,
    ],
    contentPlugin: content,
    storagePlugin: storage,
    secureStoragePlugin: storage,
    dependencyInjectionPlugin: dependency,
    routeMapBuilder: () => mystiq.MystiqRouteMap(
      routes: {
        '/': (route) => ui.ContentPage(path: '/home'),
      },
    ),
  );
}
